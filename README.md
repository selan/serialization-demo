## Introduction

This is a simple demonstration of how **serialization** works in C++. It involves a two-step process: first, it flattens object(s) into a one-dimensional stream of bytes and sends them to a destination (such as a disk or a connection), and then it reads the same source to convert that stream of bytes back into the original object(s).

The first function, `write_to_disk()`, sends to a binary file a `SBoard` object, which is essentially a 9x9 matrix of shorts, followed by a list of 2 _commands_.

A `Command` object comprises a numeric code representing an issued command and a list of 4 parameters (or arguments) for that command.

Subsequently, we call another function, `read_from_disk()`, which reads the same information from the binary file back into new variables to demonstrate that the serialization process works.

At the end of each function, I print out the data in a readable form, allowing us to verify whether the serialization has worked or not.

## Copyright

[Selan Santos](mailto:selan.santos@ufrn.br), DIMAp, 2023 :copyright:

_October, 2023_
