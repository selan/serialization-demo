/**
 * This is a simple demo of how serialization works in C++.
 *
 * To compile: clang++ -Wall -std=c++17 serialize.cpp -o serialize
 *
 * @author Selan
 * @date October 11th, 2023
 */

#include <array>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>

// Alias to improve code reading.
using byte = char;

/// This struct represents a prototype of a Sudoku board.
struct SBoard {
  // Matrix size.
  static constexpr short length = 9;
  std::array<std::array<short, length>, length> m_board;

  /// Returns the board representation as a string.
  std::string to_string() {
    std::ostringstream oss;
    for (const auto& row : m_board) {
      oss << "| ";
      for (const auto& item : row) {
        oss << std::setw(2) << item << " ";
      }
      oss << "|\n";
    }
    return oss.str();
  }

  /// Fill up the matrix with number, from 1 to size x size.
  SBoard() {
    short ct{ 1 };
    for (auto& row : m_board) {
      for (auto& item : row) {
        item = ct++;
      }
    }
  }
};

/// This struct represents a prototype of a Sudoku command.
struct Command {
  /// Enumeration of possible commands
  enum command_t : short { PLACE = 0, REMOVE, NONE };
  // static constexpr short n_cmds=3;
  command_t operation;         //!< Código da operação
  std::array<short, 4> param;  //!< Parâmetros do comando

  /// Retorna uma representação do comandos na forma de uma string.
  [[nodiscard]] std::string to_string() const {
    // List of names for each command, for debugging (same order as in the enumeration).
    static std::unordered_map<command_t, std::string_view> cmd_names{ { PLACE, "place" },
                                                                      { REMOVE, "remove" },
                                                                      { NONE, "no command" } };
    std::ostringstream oss;
    oss << "< " << std::quoted(cmd_names.at(operation)) << ", ";
    for (const auto& pa : param) {
      oss << pa << ", ";
    }
    oss << ">";
    return oss.str();
  }
  /// Ctro.
  Command(command_t op = NONE, short p1 = 0, short p2 = 0, short p3 = 0, short p4 = 0)
      : operation{ op }, param{ p1, p2, p3, p4 } {}
  ~Command() = default;
};

/// ===============[ WRITING TO DISK ] ========================
void write_to_disk(const std::string& filename) {
  // Simulating a log of plays made by the player.
  std::array<Command, 2> log_of_commands{
    Command(Command::PLACE, 1, 2, 5, 0),
    Command(Command::REMOVE, 4, 7, -1, -1),
  };
  // Simulating a Sudoku board.
  SBoard board;

  std::cout << "--> Original Data:\n";
  std::cout << "The board: \n" << board.to_string();
  int ct_cmd{ 0 };
  for (const auto& cmd : log_of_commands) {
    std::cout << "cmd #" << ct_cmd++ << ": " << cmd.to_string() << '\n';
  }
  std::cout << "\n";

  std::cout << ">>> Writing data to disk...\n";
  // [1] Open output file
  std::ofstream ofs{ filename, std::ios::binary };
  if (not ofs) {
    std::cout << ">>> Sorry, could open output file " << std::quoted(filename) << ". Aborting...\n";
    return;
  }
  // [2] Send the matrix first.
  ofs.write((byte*)&board, sizeof(SBoard));
  // [3] Send the # of commands.
  size_t n_cmds = log_of_commands.size();
  ofs.write((byte*)&n_cmds, sizeof(size_t));
  // [4] Send each command.
  for (const auto& cmd : log_of_commands) {
    ofs.write((byte*)&cmd, sizeof(Command));
  }
  // [5] Close file
  ofs.close();
  std::cout << "<<< Data saved successfuly!\n\n";
}

/// ===============[ READING FROM DISK ] ========================
void read_from_disk(const std::string& filename) {
  // This vector will store the commands read from the input file.
  std::vector<Command> commands_loaded;
  SBoard board_loaded;  // This will be filled in with data from the input file.

  std::cout << ">>> Reading data from disk...\n";
  // [1] Open input file.
  std::ifstream ifs{ filename, std::ios::binary };
  if (not ifs) {
    std::cout << ">>> Sorry, could open input file " << std::quoted(filename) << ". Aborting...\n";
    return;
  }
  // [2] Read the matrix first.
  ifs.read((byte*)&board_loaded, sizeof(SBoard));
  // [3] Read the number of commands that has been stored in the file.
  size_t n_cmds = 0;
  ifs.read((byte*)&n_cmds, sizeof(size_t));
  // Resize vector so that it can hold the commands we're about to read.
  commands_loaded.resize(n_cmds);
  // [4] Read the commands from file.
  size_t ct_cmds{ 0 };
  while (ct_cmds < n_cmds) {
    ifs.read((byte*)&commands_loaded[ct_cmds++], sizeof(Command));
  }
  // [5] Close the input file.
  ifs.close();
  std::cout << "<<< Data successfuly read from file!\n\n";

  std::cout << "--> This is the data retrieved from the disk:\n";
  std::cout << "The board:\n" << board_loaded.to_string();
  std::cout << "# of commands: " << n_cmds << "\n";
  // Print the commands so we can see if they are what we expect.
  ct_cmds = 0;
  for (const auto& cmd : commands_loaded) {
    std::cout << "cmd #" << ct_cmds++ << ": " << cmd.to_string() << '\n';
  }
}

int main() {
  const std::string filename{ "match.sdk" };
  write_to_disk(filename);
  read_from_disk(filename);
  return 0;
}
